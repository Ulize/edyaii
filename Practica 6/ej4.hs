import Par

data T a = E | N (T a) a (T a) deriving Show

altura :: T a -> Int
altura E = 0
altura (N l x r) = 1 + max (altura l) (altura r)

combinar :: T a -> T a -> T a
combinar E r = r
combinar (N l' x r') r = N (combinar l' r') x r

filterT :: (a -> Bool) -> T a -> T a
filterT _ E = E
filterT p (N l x r) = let (l', r') = (filterT p l) ||| (filterT p r)
                      in if p x 
                         then N l' x r'
                         else combinar l' r'

quicksortT :: T Int -> T Int
quicksortT E = E
quicksortT t@(N l a r) = let (l', r') = quicksortT (filterT (<a) t) ||| quicksortT (filterT (>a) t)
                         in N l' a r'

inorder :: T a -> [a] 
inorder E = []
inorder (N l a r) = let (l', r') = inorder l ||| inorder r
                    in l' ++ [a] ++ r'