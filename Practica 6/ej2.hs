import Par

data Tree a = E | Leaf a | Join (Tree a) (Tree a)

mapreduce :: (a -> b) -> (b -> b -> b) -> b -> Tree a -> b
mapreduce f g e E = e
mapreduce f g e (Leaf a) = f a
mapreduce f g e (Join l r) = let (l', r') = mapreduce f g e l ||| mapreduce f g e r
                             in g l' r'

mcss :: (Num a, Ord a) => Tree a -> a
mcss t = fst' (mapreduce f' g' (0, 0, 0, 0) t) where
             fst' (a, _, _, _) = a
             f' b = let b' = max b 0 in (b', b', b', b)
             g' (x, y, z, w) (b, c, d, e) = (maximum [x, b, z + c], max y (w + c), max d (e + z) , w + e) 
