import Par

data BTree a = Empty | Node Int (BTree a) a (BTree a) deriving Show

nodes :: BTree a -> Int
nodes Empty = 0
nodes (Node t _ _ _) = t

nth :: BTree a -> Int -> a {-calcula el n-esimo elemento de una secuencia-}
nth (Node _ l a r) n | nodes l == n = a
                     | nodes l < n = nth r (n-nodes l-1)
                     | nodes l > n = nth l n

cons :: a -> BTree a -> BTree a {-la cual inserta un elemento al comienzo de la secuencia.-}
cons a Empty = Node 1 Empty a Empty
cons a (Node n l b r) = Node (n + 1) (cons a l) b r 

tabulate :: (Int -> a) -> Int -> BTree a {-la cual dada una funcion f y un entero n devuelve una secuencia de
tamano n, donde cada elemento de la secuencia es el resultado de aplicar f al indice del elemento.-}
tabulate f 0 = Empty
tabulate f n = let m = div n 2
                   f' = \x -> f (x + m + 1)
                   (l, r) = if mod n 2 == 1 
                            then (tabulate f m) ||| (tabulate f' m)
                            else (tabulate f m) ||| (tabulate f' (m - 1))
               in Node n l (f m) r

map' :: (a -> b) -> BTree a -> BTree b {-la cual dada una funci´on f y una secuencia s, devuelve el resultado de
aplicar f sobre cada elemento de s.-}
map' f Empty = Empty
map' f (Node n l a r) = let (l', r') = map' f l ||| map' f r
                        in Node n l' (f a) r'

take' :: Int -> BTree a -> BTree a {-tal que dados un entero n y una secuencia s devuelve los primeros n
elementos de s.-}
take' 0 _ = Empty
take' _ Empty = Empty
take' n (Node m l a r) | nodes l >  n = take' n l
                       | nodes l <  n = Node n l a (take' (n - nodes l - 1) r)
                       | nodes l == n = l

drop' :: Int -> BTree a -> BTree a {-tal que dados un entero n y una secuencia s devuelve la secuencia s sin los
primeros n elementos.-}
drop' 0 s = s
drop' _ Empty = Empty
drop' n (Node m l a r) | nodes l >  n = Node (m - n) (drop' n l) a r
                       | nodes l <  n = drop' (n - nodes l - 1) r
                       | nodes l == n = Node (m-n) Empty a r

inorder :: BTree a -> [a] {-tal que dada una secuencia, devuelve su forma de lista-}
inorder Empty = []
inorder (Node _ l a r) = let (l', r') = inorder l ||| inorder r
                         in l' ++ [a] ++ r'