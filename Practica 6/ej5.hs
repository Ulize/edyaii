import Par

data BTree a = Empty | Node Int (BTree a) a (BTree a) deriving Show

nodes :: BTree a -> Int
nodes Empty = 0
nodes (Node s _ _ _) = s

take' :: Int -> BTree a -> BTree a {-tal que dados un entero n y una secuencia s devuelve los primeros n
elementos de s.-}
take' 0 _ = Empty
take' _ Empty = Empty
take' n (Node m l a r) | nodes l >  n = take' n l
                       | nodes l <  n = Node n l a (take' (n - nodes l - 1) r)
                       | nodes l == n = l

drop' :: Int -> BTree a -> BTree a {-tal que dados un entero n y una secuencia s devuelve la secuencia s sin los
primeros n elementos.-}
drop' 0 s = s
drop' _ Empty = Empty
drop' n (Node m l a r) | nodes l >  n = Node (m - n) (drop' n l) a r
                       | nodes l <  n = drop' (n - nodes l - 1) r
                       | nodes l == n = Node (m-n) Empty a r

splitAt :: BTree a -> Int -> (BTree a, BTree a)
splitAt t i = take' i t ||| drop' i t

rotateL :: BTree a -> BTree a
rotateL (Node n l x (Node m l' x' r)) = Node n (Node (m - nodes r + nodes l) l x l') x' r

rotateR :: BTree a -> BTree a
rotateR (Node n (Node m l x r) x' r') = Node n l x (Node (m - nodes l + nodes r') r x' r')

rebalance :: BTree a -> BTree a
rebalance Empty = Empty
rebalance t@(Node n l x r) | abs (nodes l - nodes r) <= 1 = Node n (rebalance l) x (rebalance r)
                           | otherwise = if nodes l < nodes r
                                         then rebalance (rotateL t)
                                         else rebalance (rotateR t)