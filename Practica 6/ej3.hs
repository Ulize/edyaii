import Par

data Tree a = E | Leaf a | Join (Tree a) (Tree a) deriving Show

mapReduceT :: (a -> b) -> (b -> b -> b) -> b -> Tree a -> b
mapReduceT _ _ e E = e
mapReduceT f _ _ (Leaf x) = f x
mapReduceT f g e (Join l r) = let (l', r') = mapReduceT f g e l ||| mapReduceT f g e r
                              in g l' r'

reduceT :: (a -> a -> a) -> a -> Tree a -> a
reduceT _ e E = e
reduceT _ _ (Leaf x) = x
reduceT f e (Join l r) = let (l', r') = reduceT f e l ||| reduceT f e r
                         in f l' r'

mapT :: (a -> b) -> Tree a -> Tree b
mapT _ E = E
mapT f (Leaf x) = Leaf (f x)
mapT f (Join l r) = let (l', r') = mapT f l ||| mapT f r
                    in Join l' r'

sufijos :: Tree Int -> Tree (Tree Int)
sufijos t = sufijos' t E where
            sufijos' (Leaf a) s = Leaf s
            sufijos' (Join l r) E = Join (sufijos' l r) (sufijos' r E)
            sufijos' (Join l r) s = Join (sufijos' l (Join r s)) (sufijos' r s)

zipT :: Tree a -> Tree b -> Tree (a,b)
zipT (Leaf x) (Leaf y) = Leaf (x, y)
zipT (Join l r) (Join l' r') = let (l'', r'') = (zipT l l') ||| (zipT r r')
                               in Join l'' r''
zipT _ _ = E

conSufijos :: Tree Int -> Tree (Int, Tree Int)
conSufijos t = zipT t (sufijos t)

maxT :: Tree Int -> Int
maxT = reduceT max 0

maxAll :: Tree (Tree Int) -> Int
maxAll = mapReduceT maxT max (minBound :: Int)

mejorGanancia :: Tree Int -> Int
mejorGanancia t = maxT (mapT (\(x, t) -> maxT t - x) (conSufijos t))