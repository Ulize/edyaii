import Seq
import Par
import qualified Arr as A

dyc :: Seq s => s a -> b -> (a -> b) -> (b -> b -> b) -> b
dyc s v b c = case showtS s of
                EMPTY -> v
                ELT v' -> b v'
                NODE l r -> let (l', r') = dyc l v b c ||| dyc r v b c
                            in c l' r'

contractS :: Seq s => (a -> a -> a) -> s a -> s a
contractS f s = case even dim of
  True -> tabulateS f' (div dim 2)
  False -> tabulateS f'' ((div dim 2) + 1) 
  where dim = lengthS s
        f' i = f (nthS s (2 * i)) (nthS s ((2 * i) + 1))
        f'' i | i < (div dim 2) = f (nthS s (2 * i)) (nthS s ((2 * i) + 1))
              | otherwise = nthS s (dim - 1)

instance Seq A.Arr where
  emptyS        = A.empty
  singletonS a  = fromList [a]
  lengthS       = A.length
  nthS          = (A.!)
  tabulateS     = A.tabulate
  mapS f s      = tabulateS (f.nthS s) (lengthS s)
  filterS f     = joinS.(mapS (\a -> if f a then singletonS a else emptyS)) 
  appendS s1 s2 = tabulateS (choose s1 s2) (lengthS s1 + lengthS s2) where
                    choose s s' i | i < lengthS s = nthS s i
                                  | otherwise = nthS s' (i - lengthS s)
  takeS         = (A.subArray 0)
  dropS i s     = A.subArray i ((lengthS s) - i) s
  showtS s      = case lengthS s of
                    0 -> EMPTY
                    1 -> ELT (nthS s 0)
                    n -> NODE (takeS (div n 2) s) (dropS (div n 2) s)
  showlS s      = case lengthS s of
                    0 -> NIL
                    n -> CONS (nthS s 0) (dropS 1 s)
  joinS         = A.flatten
  reduceS f e s = case lengthS s of
                    1 -> f e (nthS s 0)
                    n -> reduceS f e (contractS f s)
  scanS f e s = case lengthS s of
                  0 -> (singletonS e, e)
                  1 -> let funApplied = f e (nthS s 0)
                       in (appendS (singletonS e) (singletonS funApplied), funApplied)
                  n -> let (s',r) = scanS f e (contractS f s)
                       in (tabulateS (\i -> if even i then nthS s' (div i 2) else f (nthS s' (div i 2)) (nthS s (i-1))) (lengthS s), r)   
  fromList      = A.fromList