module Lab01 where

smallest (x, y, z) | x <= y && x <= z = x
                   | y <= x && y <= z = y
                   | z <= x && z <= y = z

smallest2 :: (Ord a, Num a) => (a, a, a) -> a
smallest2 = \(x,y,z) -> if x <= y && x <= z then x else if y <= x && y <= z then y else z