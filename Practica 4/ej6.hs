type Rank = Int
data Heap a = E | N Rank a (Heap a) (Heap a)

rank :: Heap a -> Rank
rank E = 0
rank(N r _ _ _) = r

makeH x a b = if rank a > rank b
              then N (rank b+1) x a b
              else N (rank a+1) x b a

merge :: Ord a => Heap a -> Heap a -> Heap a
merge h1 E = h1
merge E h2 = h2
merge h1@(N x a1 b1) h2@(Ny a2 b2) = if x <= y
                                     then makeH x a1 (merge b1 h2)
                                     else makeH y a2 (merge h1 b2)


toHeap :: a -> Heap a
toHeap a = N 1 a E E

fromList :: [a] -> Heap a
fromList [] = []
fromList l = foldl merge (map toHeap a)
