data T123 a = E123 |
              T2 a (T123 a) (T123 a) |
              T3 a a (T123 a) (T123 a) (T123 a)|
              T4 a a a (T123 a) (T123 a) (T123 a) (T123 a)

data Color = R | B
data RBT a = E | T Color (RBT a) a (RBT a)

rbtTo123 :: RBT a -> T123 a
rbtTo123 E = E123
rbtTo123 (T B (T R l1 a1 l2) a2 (T R r1 a3 r2) ) = T4 a1 a2 a3 (rbtTo123 l1) (rbtTo123 l2) (rbtTo123 r1) (rbtTo123 r2)
rbtTo123 (T B (T R l1 a1 l2) a2 r) = T3 a1 a2 (rbtTo123 l1) (rbtTo123 l2) (rbtTo123 r)
rbtTo123 (T B l a2 (T R r1 a1 r2)) = T3 a1 a2 (rbtTo123 l) (rbtTo123 r1) (rbtTo123 r2) 
rbtTo123 (T B l a r) = T2 a (rbtTo123 l) (rbtTo123 r)
