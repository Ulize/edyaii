data PHeaps a = Empty | Root a [PHeaps a]

--1
auxmin :: a -> PHeaps a -> a
auxmin a Empty = a
root a (Root b _) = if a < b then a else b

isPHeap :: Ord a => PHeaps a -> Bool
isPHeap Empty = True
isPHeap (Root a l) = (a <= foldl auxmin a l) && (foldl (&&) True (map isPHeap l))

--2
merge :: Ord a => PHeaps a -> PHeaps a -> PHeaps a
merge Empty h2 = h2
merge h1 Empty = h1
merge h1@(Root a1 l1) h2@(Root a2 l2) = if a1 < a2
                                        then Root a1 (h2:l1)
                                        else Root a2 (h1:l2)

--3
insert :: Ord a => PHeaps a -> a -> PHeaps a
insert h a = merge h (Root a [])

--4
concatHeaps :: Ord a => [PHeaps a] -> PHeaps a
concatHeaps [] = Empty
concatHeaps l = foldl merge Empty l

--5
aux :: Ord a => PHeaps a -> PHeaps a -> PHeaps a
aux Empty b = b
aux a Empty = a
aux t1@(Root a _) t2@(Root b _) = if a < b then t1 else t2

delMin :: Ord a => PHeaps a -> Maybe (a, PHeaps a)
delMin Empty = Nothing
delMin (Root a hijos) = Just (a, foldl merge Empty hijos)