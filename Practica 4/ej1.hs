data Tree a = EB | Node (Tree a) a (Tree a) deriving Show
--a
completo :: a -> Int -> Tree a
completo _ (-1) = EB
completo x d = Node (completo x (d-1)) x (completo x (d-1))

--b
balanceado::a -> Int -> Tree a
balanceado _ 0 = EB
balanceado x n = if mod n 2 == 0
                 then Node (balanceado x (div (n+1) 2)) x (balanceado x (div (n-1) 2))
                 else Node (balanceado x (div n 2)) x (balanceado x (div n 2))
