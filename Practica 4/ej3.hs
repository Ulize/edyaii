data BST a = Hoja | Nodo (BST a) a (BST a)
member :: Eq a => a -> BST a -> Bool
member _ Hoja = False
member a (Nodo l b r) | a < b = member a l
                      | a > b = member a r
                      | otherwise = True
