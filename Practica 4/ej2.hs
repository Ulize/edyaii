data BST a = Hoja | Nodo (BST a) a (BST a) deriving Show
--1
maxim :: Ord a => BST a -> a
maxim Hoja = undefined
maxim (Nodo _ x Hoja) = x
maxim (Nodo _ _ tree) = maxim tree
--2
minim :: Ord a => BST a -> a
minim Hoja = undefined
minim (Nodo Hoja x _) = x
minim (Nodo tree _ _) = maxim tree

checkBST :: Ord a => BST a -> Bool
checkBST Hoja = True
checkBST (Nodo l x r) = (maxim l < x)
                        && (minim r > x)
                        && (checkBST l)
                        && (checkBST r)

--3
insert :: Ord a => a -> BST a -> BST a
insert a Hoja = Nodo Hoja a Hoja
insert a (Nodo l b r) | a <= b = Nodo (insert a l) b r
                      | otherwise = Nodo l b (insert a r)


menoresiguales :: Ord a => BST a -> a -> BST a
menoresiguales Hoja _ = Hoja
menoresiguales (Nodo l b r) x | x > b = Nodo l b (menoresiguales r x)
                              | x < b = menoresiguales l x
                              | otherwise = insert b l

mayores :: Ord a => BST a -> a -> BST a
mayores Hoja _ = Hoja
mayores (Nodo l b r) x | x > b = mayores r x
                       | x < b = Nodo (mayores l x) b r
                       | otherwise = r

splitBST :: Ord a => BST a -> a -> (BST a, BST a)
splitBST Hoja _ = (Hoja, Hoja)
splitBST t x = (menoresiguales t x, mayores t x)

--4 checkear
join :: Ord a => BST a -> BST a -> BST a
join Hoja t = t
join t Hoja = t
join t1@(Nodo l1 a r1) t2@(Nodo l2 b r2) | a < b = Nodo (join l2 (menoresiguales t1 b)) b (join r2 (mayores t1 b))
                                         | a > b = Nodo (join l1 (menoresiguales t2 a)) a (join r1 (mayores t2 a))
                                         | otherwise = Nodo (join l1 l2) a (join r1 r2)
