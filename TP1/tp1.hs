{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

data TTree k v = Node k (Maybe v) (TTree k v) (TTree k v) (TTree k v)
                | Leaf k v
                | E deriving Show

search :: Ord k => [k] -> TTree k v -> Maybe v
search [] _ = Nothing
search _ E = Nothing
search [x] (Leaf k v) = if k == x then (Just v) else Nothing
search (x:xs) (Leaf _ _) = Nothing
search [x] (Node k v t1 _ t3) | x < k = search [x] t1
                              | x == k = v
                              | otherwise = search [x] t3
search l@(x:xs) (Node k _ t1 t2 t3) | x < k = search l t1
                                    | x == k = search xs t2
                                    | otherwise = search l t3

insert :: Ord k => [k] -> v -> TTree k v -> TTree k v
insert [] v t = t
insert [x] v E = Leaf x v
insert (x:xs) v E = Node x Nothing E (insert xs v E) E
insert [x] v (Leaf k v1) | x < k = Node k (Just v1) (Leaf x v) E E
                         | x == k = Leaf k v
                         | otherwise = Node k (Just v1) E E (Leaf x v)
insert (x:xs) v (Leaf k v1) | x < k = Node k (Just v1) (insert xs v E) E E
                            | x == k = Leaf k v
                            | otherwise = Node k (Just v1) E E (insert xs v E)
insert [x] v (Node k v1 t1 t2 t3) | x < k = Node k v1 (insert [x] v t1) t2 t3
                                  | x == k = Node k (Just v) t1 t2 t3
                                  | otherwise = Node k v1 t1 t2 (insert [x] v t3)
insert (x:xs) v (Node k v1 t1 t2 t3) | x < k = Node k v1 (insert xs v t1) t2 t3
                                     | x == k = Node k v1 t1 (insert xs v t2) t3
                                     | otherwise = Node k v1 t1 t2 (insert xs v t3)

fix :: Ord k => TTree k v -> TTree k v
fix t = case t of
             (Node k Nothing E E E) -> E
             (Node k Nothing l E E) -> l
             (Node k Nothing E E r) -> r
             (Node k (Just v) E E E) -> (Leaf k v)
             t -> t

delete :: Ord k => [k] -> TTree k v -> TTree k v
delete [] t = t
delete _ E = E
delete [x] l@(Leaf k v) = if k == x then E else l
delete (x:xs) l@(Leaf k v) = l
delete [x] n@(Node k Nothing t1 t2 t3) | x < k = fix (Node k Nothing (delete [x] t1) t2 t3)
                                       | x == k = n
                                       | otherwise = fix (Node k Nothing t1 t2 (delete [x] t3))
delete (x:xs) (Node k Nothing t1 t2 t3) | x < k = fix (Node k Nothing (delete (x:xs) t1) t2 t3)
                                        | x == k = fix (Node k Nothing t1 (delete xs t2) t3)
                                        | otherwise = fix (Node k Nothing t1 t2 (delete (x:xs) t3))
delete [x] (Node k v t1 t2 t3) | x < k = fix (Node k v (delete [x] t1) t2 t3)
                               | x == k = fix (Node k Nothing t1 t2 t3)
                               | otherwise = fix (Node k v t1 t2 (delete [x] t3))
delete (x:xs) (Node k v t1 t2 t3) | x < k = fix (Node k v (delete (x:xs) t1) t2 t3)
                                  | x == k = fix (Node k v t1 (delete xs t2) t3)
                                  | otherwise = fix (Node k v t1 t2 (delete (x:xs) t3))

keys :: TTree k v -> [[k]]
keys E = []
keys (Leaf k v) = [[k]]
keys (Node k Nothing t1 t2 t3) = (keys t1) ++ (map (k:) (keys t2)) ++ (keys t3)
keys (Node k (Just v) t1 t2 t3) = (keys t1) ++ [[k]] ++ (map (k:) (keys t2)) ++ (keys t3)


add :: k -> ([k], v) -> ([k], v)
add k (ks, v) = (k:ks, v)

keys2 :: TTree k v -> [([k], v)]
keys2 E = []
keys2 (Leaf k v) = [([k],v)]
keys2 (Node k Nothing t1 t2 t3) = (keys2 t1) ++ (map (add k) (keys2 t2)) ++ (keys2 t3)
keys2 (Node k (Just v) t1 t2 t3) = (keys2 t1) ++ [([k], v)] ++ (map (add k) (keys2 t2)) ++ (keys2 t3)

class Dic k v d | d -> k v where
  vacio :: d
  insertar :: Ord k => k -> v -> d -> d
  buscar :: Ord k => k -> d -> Maybe v
  eliminar :: Ord k => k -> d -> d
  claves :: Ord k => d -> [(k, v)]

instance Ord k => Dic [k] v (TTree k v) where
  vacio = E
  insertar = insert
  buscar = search
  eliminar = delete
  claves = keys2
