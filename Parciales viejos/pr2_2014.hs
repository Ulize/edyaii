import Par
import Seq
import ArrSeq
import qualified Arr as A

data Tree a = E | L a | N Int (Tree a) (Tree a) deriving Show

concatT :: Tree (Tree a) -> Tree a
concatT E = E
concatT (L t) = t
concatT (N _ l r) = let (l', r') = concatT l ||| concatT r
                   in case l' of
                     E -> r'
                     (L t) -> case r' of
                           E -> L t
                           (L _) -> (N 2 l' r')
                           (N n' _ _) -> (N (n'+1) l' r')
                     (N n _ _) -> case r of
                           E -> l'
                           (L _) -> (N (n+1) l' r')
                           (N n' l r) -> (N (n+n) l' r')
                   = let (l', r') = concat l ||| concat r
                     in J (size l'+size r') l' r'
