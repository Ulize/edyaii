data Treap p k = E | N (Treap p k) p k (Treap p k)

key :: Treap p k -> k
key E = undefined
key (N _ _ k _) = k

priority :: Treap p k -> p
priority E = undefined
priority (N _ p _ _) = p

isTreap :: (Ord k, Ord p) => Treap p k -> Bool
isTreap E = True
isTreap (N l p k r) = (lowestk r k) && (greatestk l k) &&
                      (lowerp l p) && (lowerp r p) &&
                      (isTreap l) && (isTreap r) where
                        lowestk E _ = True
                        lowestk (N E _ k' _) k = k' > k
                        lowestk (N l _ _ _) k = lowestk l k

                        greatestk E _ = True
                        greatestk (N _ _ k' E) k = k' < k
                        greatestk (N _ _ _ r) k = greatestk r k

                        lowerp E _ = True
                        lowerp (N _ p' _ _) p = p' < p 

rotateL (N l' p' k' (N l p k r)) = N (N l' p' k' l) p k r
rotateR (N (N l p k r) p' k' r') = N l p k (N r p' k' r')

insert :: (Ord k, Ord p) => p -> k -> Treap p k -> Treap p k
insert p k E = (N E p k E)
insert p k (N l p' k' r) | k < k' = check (N (insert p k l) p' k' r)
                         | otherwise = check (N l p' k' (insert p k r)) where
                           check n@(N _ p' _ (N _ p _ _)) = if p < p' then rotateL n else n
                           check n@(N (N _ p _ _) p' _ _) = if p < p' then rotateR n else n

split :: (Ord k, Ord p, Num p) => k -> Treap p k -> (Treap p k, Treap p k)
split _ E = (E, E)
split k n@(N _ p _ _) = let (N l _ _ r) = insert (p + 1) k n in (l, r)
