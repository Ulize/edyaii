module Lab1C where

import Data.List
import Data.Ord
import Data.Function

type Texto = String

{-
Definir una función que dado un caracter y un texto calcule la frecuencia
con la que ocurre el caracter en el texto
Por ejemplo: frecuency 'a' "casa" = 0.5
-}

frecuency :: Char -> Texto -> Float
frecuency a xs = (fromIntegral (frec a xs) ) / (fromIntegral (length xs) )

frec _ [] = 0
frec c (x:xs) | c == x = 1 + frec c xs
              | otherwise = frec c xs

{-
Definir una función frecuencyMap que dado un texto calcule la frecuencia
con la que ocurre cada caracter del texto en éste.
La lista resultado debe estar ordenada respecto a la frecuencia con la que ocurre
cada caracter, de menor a mayor frecuencia.

Por ejemplo: frecuencyMap "casa" = [('c',0.25),('s',0.25),('a',0.5)]

-}
unique xs = [x | (x,i) <- zip xs [0..], not (elem x (take i xs))]

aux [] ys = []
aux (x:xs) ys = ( x , (frecuency x ys) ): (aux xs ys )

f (_, a) (_, b) | a < b = LT
                | a == b = EQ
                | otherwise = GT

frecuencyMap :: Texto -> [(Char, Float)]
frecuencyMap xs = sortBy f (aux (unique xs) xs)

{-frecuencyMap xs = let ys = unique xs in zip ys map frecuency ys)

Definir una función subconjuntos, que dada una lista xs devuelva una lista
con las listas que pueden generarse con los elementos de xs.

Por ejemplo: subconjuntos [2,3,4] = [[2,3,4],[2,3],[2,4],[2],[3,4],[3],[4],[]]
-}

subconjuntos :: [a] -> [[a]]
subconjuntos xs = subconjuntosA xs [[]]

subconjuntosA [] yss = yss
subconjuntosA (x:xs) yss = subconjuntosA xs ( [x:ys| ys <- yss]++yss)

{-
Definir una función intercala :: a -> [a] -> [[a]]
tal que (intercala x ys) contiene las listas que se obtienen
intercalando x entre los elementos de ys.

Por ejemplo: intercala 1 [2,3] == [[1,2,3],[2,1,3],[2,3,1]]
-}

intercalaA :: a -> [a] -> [a] -> [[a]]
intercalaA a [] ys = [ys ++ [a]]
intercalaA a (x:xs) ys = (ys++[a]++(x:xs)) : (intercalaA a xs (ys++[x])) 

intercala :: a -> [a] -> [[a]]
intercala a xs = intercalaA a xs []

{-
Definir una función permutaciones que dada una lista calcule todas las permutaciones
posibles de sus elementos. Ayuda: la función anterior puede ser útil.

Por ejemplo: permutaciones "abc" = ["abc","bac","cba","bca","cab","acb"]
-}

permutaciones :: [a] -> [[a]]
permutaciones [] = [[]]
permutaciones (x:xs) = concat (map (intercala x) (permutaciones xs))