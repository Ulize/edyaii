module Lab01 where

import Data.List
import Data.Char

{-
1) Corregir los siguientes programas de modo que sean aceptados por GHCi.
-}

-- a)
not b = case b of
             True -> False
             False -> True

-- b)
ind [x]         =  []
ind (x:xs)      =  x : ind xs
ind []          =  error "empty list"

-- c)
ength []        =  0
ength (_:l)     =  1 + ength l

-- d)
list123 = 1 : 2 : 3 : []

-- e)
[]     ++! ys = ys
(x:xs) ++! ys = x : xs ++! ys

-- f)
addToTail x xs = map (+x) (tail xs)

-- g)
listmin xs = (head . sort) xs

-- h) (*)
smap f [] = []
smap f [x] = [f x]
smap f (x:xs) = f x : smap f xs

{-
2. Definir las siguientes funciones y determinar su tipo:
-}

{-
a) five, que dado cualquier valor, devuelve 5
five :: t -> int
-}
five x = 5

{-
b) apply, que toma una función y un valor, y devuelve el resultado de
aplicar la función al valor dado

apply :: (t1 -> t) -> t1 -> t
-}
apply g x = g x

{-
c) ident, la función identidad
ident :: t -> t
-}
ident x = x
{-
d) first, que toma un par ordenado, y devuelve su primera componente
first :: (t1,t2) -> t1
-}
first (x,y) = x
{-
e) derive, que aproxima la derivada de una función dada en un punto dado
derive :: Fractional a, Fractional t => (t -> a) -> t -> a
-}
derive f x = (f (x + 0.0001) - f x) / 0.0001

{-
f) sign, la función signo
sign :: Num a => a -> a
-}
sign x | x>0 = 1
       | x<0 = -1
       | otherwise = 0

{-
g) vabs, la función valor absoluto (usando sign y sin usarla)
vabs :: Num a => a -> a
-}
vabs x | sign x == -1 = -x
       | otherwise = x

vabs2 x | x < 0 = -x
        | otherwise = x

{-
h) pot, que toma un entero y un número, y devuelve el resultado de
elevar el segundo a la potencia dada por el primero
pot :: Num a, Int t => t -> a -> a
-}

pot 0 a = 1
pot x a = a * (pot (x-1) a)

{-
i) xor, el operador de disyunción exclusiva
xor :: bool -> bool -> bool
-}
xor a b | a == b = False
        | otherwise = True

{-
j) max3, que toma tres números enteros y devuelve el máximo entre ellos
max3 :: Int -> Int -> Int -> Int
-}

max3 a b c | (a > b) && (a > c) = a
           | (b > a) && (b > c) = b
           | otherwise = c

{-
k) swap, que toma un par y devuelve el par con sus componentes invertidas
swap :: (t, t1) -> (t1, t)
-}
swap (a,b) = (b,a)

{-
3) Definir una función que determine si un año es bisiesto o no, de
acuerdo a la siguiente definición:

año bisiesto 1. m. El que tiene un día más que el año común, añadido al mes de febrero. Se repite
cada cuatro años, a excepción del último de cada siglo cuyo número de centenas no sea múltiplo
de cuatro. (Diccionario de la Real Academia Espaola, 22ª ed.)
-}

bisiesto x = ((mod x 4) == 0) && (Prelude.not ((mod x 400) == 0))

{-

¿Cuál es el tipo de la función definida?
-}
bisiesto :: Int -> Bool

{-
4)

Defina un operador infijo *$ que implemente la multiplicación de un
vector por un escalar. Representaremos a los vectores mediante listas
de Haskell. Así, dada una lista ns y un número n, el valor ns *$ n
debe ser igual a la lista ns con todos sus elementos multiplicados por
n. Por ejemplo,

[ 2, 3 ] *$ 5 == [ 10 , 15 ].

El operador *$ debe definirse de manera que la siguiente
expresión sea válida:

-}
xs *$ x = map (x*) xs

v = [1, 2, 3] *$ 2 *$ 4


{-
5) Definir las siguientes funciones usando listas por comprensión:

a) 'divisors', que dado un entero positivo 'x' devuelve la
lista de los divisores de 'x' (o la lista vacía si el entero no es positivo)
-}
--divisors :: Int -> [Int]
divisors x = [y | y <- [1..(div x 2)], (mod x y) == 0 ]

{-
b) 'matches', que dados un entero 'x' y una lista de enteros descarta
de la lista los elementos distintos a 'x'
-}
matches x xs = [y | y <- xs, y == x]

{-
c) 'cuadrupla', que dado un entero 'n', devuelve todas las cuadruplas
'(a,b,c,d)' que satisfacen a^2 + b^2 = c^2 + d^2,
donde 0 <= a, b, c, d <= 'n'
-}
cuadrupla n = [(a,b,c,d) | a <- [1..n], b <- [1..n], c <- [1..n], d <- [1..n], (a*a + b*b) == (c*c + d*d)]

{-
(d) 'unique', que dada una lista 'xs' de enteros, devuelve la lista
'xs' sin elementos repetidos
-}

unique :: [Int] -> [Int]
unique xs = [x | (x,i) <- zip xs [0..], Prelude.not (elem x (take i xs))]

{-
6) El producto escalar de dos listas de enteros de igual longitud
es la suma de los productos de los elementos sucesivos (misma
posición) de ambas listas.  Definir una función 'scalarProduct' que
devuelva el producto escalar de dos listas.

Sugerencia: Usar las funciones 'zip' y 'sum'.
-}

scalarProduct xs ys = sum [x * y | (x,y) <- zip xs ys]

{-

7) Definir mediante recursión explícita
las siguientes funciones y escribir su tipo más general:

a) 'suma', que suma todos los elementos de una lista de números
-}
suma [] = 0
suma (x:xs) = x + suma xs

{-
b) 'alguno', que devuelve True si algún elemento de una
lista de valores booleanos es True, y False en caso
contrario
-}
alguno [] = False
alguno (x:xs) | x = True
              | otherwise = alguno xs


{-
c) 'todos', que devuelve True si todos los elementos de
una lista de valores booleanos son True, y False en caso
contrario
-}

todos [] = True
todos (x:xs) | Prelude.not x = False
             | otherwise = todos xs

{-
d) 'codes', que dada una lista de caracteres, devuelve la
lista de sus ordinales
-}
codes [] = []
codes (x:xs) = ord(x) : (codes xs)

{-
e) 'restos', que calcula la lista de los restos de la
división de los elementos de una lista de números dada por otro
número dado
-}

restos [] a = []
restos (x:xs) a = (mod x a) : (restos xs a)

{-
f) 'cuadrados', que dada una lista de números, devuelva la
lista de sus cuadrados
-}

cuadrados [] = []
cuadrados (x:xs) = x * x : (cuadrados xs)

{-
g) 'longitudes', que dada una lista de listas, devuelve la
lista de sus longitudes
-}

longitudes [] = []
longitudes (x:xs) = (length x) : (longitudes xs)

{-
h) 'orden', que dada una lista de pares de números, devuelve
la lista de aquellos pares en los que la primera componente es
menor que el triple de la segunda
-}
orden [] = []
orden ((x,y):xs) = if x < y * 3 then (x,y) : (orden xs) else orden xs

{-
i) 'pares', que dada una lista de enteros, devuelve la lista
de los elementos pares
-}
pares [] = []
pares (x:xs) = if mod x 2 == 0 then x : (pares xs) else pares xs


{-
j) 'letras', que dada una lista de caracteres, devuelve la
lista de aquellos que son letras (minúsculas o mayúsculas)
-}
letras [] = []
letras (x:xs) = if isLetter x then x : (letras xs) else letras xs

{-
k) 'masDe', que dada una lista de listas 'xss' y un
número 'n', devuelve la lista de aquellas listas de 'xss'
con longitud mayor que 'n'
-}

masDe [] n = []
masDe (xs:xss) n = if length xs > n then xs : (masDe xss n) else masDe xss n