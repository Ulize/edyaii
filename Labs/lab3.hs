
module Lab02 where

{-
   Laboratorio 2
   EDyAII 2022
-}

import Data.List

-- 1) Dada la siguiente definición para representar árboles binarios:

data BTree a = Leaf a | Node (BTree a) (BTree a)

-- Definir las siguientes funciones:

-- a) altura, devuelve la altura de un árbol binario.

altura :: BTree a -> Int
altura (Leaf a) = 0
altura (Node a b) = 1 + max (altura a) (altura b)

-- b) perfecto, determina si un árbol binario es perfecto (un árbol binario es perfecto si cada nodo tiene 0 o 2 hijos
-- y todas las hojas están a la misma distancia desde la raı́z).

perfecto :: BTree a -> Bool
perfecto (Leaf a) = True
perfecto (Node a b) = (altura a == altura b) && perfecto a && perfecto b

-- c) inorder, dado un árbol binario, construye una lista con el recorrido inorder del mismo.

inorder :: BTree a -> [a]
inorder (Leaf a) = [a]
inorder (Node a b) = inorder a ++ inorder b 


-- 2) Dada las siguientes representaciones de árboles generales y de árboles binarios (con información en los nodos):

data GTree a = EG | NodeG a [GTree a] 

data BinTree a = EB | NodeB (BinTree a) a (BinTree a) deriving Show

{- Definir una función g2bt que dado un árbol nos devuelva un árbol binario de la siguiente manera:
   la función g2bt reemplaza cada nodo n del árbol general (NodeG) por un nodo n' del árbol binario (NodeB ), donde
   el hijo izquierdo de n' representa el hijo más izquierdo de n, y el hijo derecho de n' representa al hermano derecho
   de n, si existiese (observar que de esta forma, el hijo derecho de la raı́z es siempre vacı́o).
   
   
   Por ejemplo, sea t: 
       
                    A 
                 / | | \
                B  C D  E
               /|\     / \
              F G H   I   J
             /\       |
            K  L      M    
   
   g2bt t =
         
                  A
                 / 
                B 
               / \
              F   C 
             / \   \
            K   G   D
             \   \   \
              L   H   E
                     /
                    I
                   / \
                  M   J  
-}

g2btAux :: GTree a -> [GTree a] -> BinTree a
g2btAux (NodeG x []) [] = (NodeB EB x EB) 
g2btAux (NodeG x []) (y : ys) = (NodeB EB x (g2btAux y ys)) 
g2btAux (NodeG x (y:ys)) [] = (NodeB (g2btAux y ys) x EB) 
g2btAux (NodeG x (y:ys)) (z : zs) = (NodeB (g2btAux y ys) x (g2btAux z zs)) 

g2bt :: GTree a -> BinTree a
g2bt EG = EB
g2bt tree = g2btAux tree []


-- 3) Utilizando el tipo de árboles binarios definido en el ejercicio anterior, definir las siguientes funciones: 
{-
   a) dcn, que dado un árbol devuelva la lista de los elementos que se encuentran en el nivel más profundo 
      que contenga la máxima cantidad de elementos posibles. Por ejemplo, sea t:
            1
          /   \
         2     3
          \   / \
           4 5   6
                             
      dcn t = [2, 3], ya que en el primer nivel hay un elemento, en el segundo 2 siendo este número la máxima
      cantidad de elementos posibles para este nivel y en el nivel tercer hay 3 elementos siendo la cantidad máxima 4.
   -}

node :: BinTree a -> a
node EB = undefined
node (NodeB _ x _) = x

dcn :: BinTree a -> [a]
dcn EB = []
dcn (NodeB EB x _) = [x]
dcn (NodeB _ x EB) = [x]
dcn (NodeB l x r) | length ln == length rn = ln ++ rn
                  | otherwise = [node l]++[node r]
                  where ln = dcn l
                        rn = dcn r
{- b) maxn, que dado un árbol devuelva la profundidad del nivel completo
      más profundo. Por ejemplo, maxn t = 2   -}

maxn :: BinTree a -> Int
maxn EB = 0
maxn (NodeB EB _ _) = 1
maxn (NodeB _ _ EB) = 1
maxn (NodeB l x r) = 1 + min (maxn l) (maxn r)


{- c) podar, que elimine todas las ramas necesarias para transformar
      el árbol en un árbol completo con la máxima altura posible. 
      Por ejemplo,
         podar t = NodeB (NodeB EB 2 EB) 1 (NodeB EB 3 EB)
-}

podarAux :: BinTree a -> Int -> BinTree a
podarAux EB _ = EB
podarAux (NodeB EB x _) _ = (NodeB EB x EB)
podarAux (NodeB _ x EB) _ = (NodeB EB x EB)
podarAux (NodeB l x r) n | n == 1 = (NodeB EB x EB)
                         | otherwise = (NodeB (podarAux l (n-1)) x (podarAux r (n-1)))

podar :: BinTree a -> BinTree a
podar EB = EB
podar t = podarAux t (maxn t)
