import Seq
import ListSeq

--1
promedios :: Seq s => s Int -> s Float
promedios s = f 0 s' where
                s' = dropS 1 $ fst (scanS (+) 0 s)
                f i s = case showlS s of
                          NIL -> emptyS
                          CONS x xs -> appendS (singletonS $ (fromIntegral x :: Float) / (fromIntegral i :: Float))  (f (i+1) xs)
  
mayores :: Seq s => s Int -> Int
mayores s = mayoresAux (nthS s 0) s where
              mayoresAux l s = case showlS s of
                                 NIL -> 0
                                 CONS x xs -> if l < x then 1 + mayoresAux x xs
                                                       else mayoresAux l xs

--2

matrixMult :: (Int, Int, Int, Int) -> (Int, Int, Int, Int) -> (Int, Int, Int, Int)
matrixMult (a,b,c,d) (w,x,y,z) = (a*w+b*y, a*x+b*z, c*w+d*y, c*x+d*z)

fibSeq :: Seq s => Int -> s Int
fibSeq n = mapS (\(a,b,c,d) -> a) (fst (scanS matrixMult (1, 1, 1, 0) (tabulateS (\_ -> (1,1,1,0)) n)))



--3

{-
aguaHistAux :: Seq s => s Int -> s Int

aguaHist :: Seq s => s Int -> Int

ejemplo = fromList [2,3,4,7,5,2,3,2,6,4,3,5,2,1] :: [Int]
--4
data Paren = Open | Close

matchP :: Seq s => s Paren -> (Int, Int)

matchParen :: Seq s => s Paren -> Bool

-}

{-

matchPS :: Seq s => s Paren -> (Int, Int)
matchP s = 

matchParenS :: Seq s => s Paren -> Bool
matchParenS s = let (prev, res) = scanS c (0,0) (mapS b s)
                in res == (0,0) && reduceS minP
-}
                  
--5

{-
mcss :: Seq s => s Int -> Int


--mal
sccml :: Seq s => s Int -> Int

-}
{-
--6
cantMultiplos :: Seq s => s Int -> Int

--7
-}
merge :: Seq s => (a -> a -> Ordering) -> s a -> s a -> s a
merge f s t = case showlS s of
                NIL -> t
                CONS x xs -> case showlS t of
                               NIL -> s
                               CONS y ys -> if f x y == GT then appendS (singletonS x) (merge f xs t)
                                                           else appendS (singletonS y) (merge f s ys)

{-
merge _ [] t = t
merge _ s [] = s
merge f (x:xs) (y:ys) | f x y == GT = x : merge f xs (y:ys)
                      | otherwise = y : merge f (x:xs) ys
-}

sort :: Seq s => (a -> a -> Ordering) -> s a -> s a 
sort f s = dyc s emptyS singletonS (merge f)

{-

maxE :: Seq s => (a -> a -> Ordering) -> s a -> a 


maxS :: Seq s => (a -> a -> Ordering) -> s a -> a 


group:: Seq s => (a -> a -> Ordering) -> s a -> s a


-- collect :: Seq s => (a -> a -> Ordering) -> s (a, b) -> s (a, s b)


--8
datosIngreso :: Seq s => s (String, s Int) -> s (Int, Int)


--9
countCaract:: Seq s => s (s Char) -> s (Char, Int)

huffman :: Seq s => s (s Char) -> s (Int, s Char)
-}