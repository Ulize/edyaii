module ListSeq where

import Seq
import Par

contractS :: (a -> a -> a) -> [a] -> [a]
contractS _ []       = emptyS
contractS _ l@[x]    = l
contractS f s =  case s of
                    [] -> emptyS
                    xs@[x] -> xs
                    (x:y:xs) -> let (x',xs') = f x y ||| contractS f xs
                                in x':xs'

dyc :: Seq s => s a -> b -> (a -> b) -> (b -> b -> b) -> b
dyc s v b c = case showtS s of
                EMPTY -> v
                ELT v' -> b v'
                NODE l r -> let (l', r') = dyc l v b c ||| dyc r v b c
                            in c l' r'

expandS :: (a -> a -> a) -> [a] -> [a] -> [a]
expandS _ [] _ = []
expandS _ [_] ys = ys
expandS f (x:_:xs) (y:ys) = let (z, zs) = (f y x) ||| expandS f xs ys
                            in appendS (singletonS y) (appendS (singletonS z) zs)

instance Seq [] where
  emptyS            = []
  singletonS a      = [a]
  lengthS           = length
  nthS              = (!!)
  tabulateS f n = tabulateSAux f n n where
    tabulateSAux _ _ 0 = emptyS
    tabulateSAux f n m = let (x,xs) = (f (n - m)) ||| (tabulateSAux f n (m-1))
                         in appendS (singletonS x) xs
  mapS _ []         = [] 
  mapS f (x:xs)     = let (x', xs') = f x ||| mapS f xs
                      in x':xs' 
  filterS           = filter
  appendS [] ys     = ys
  appendS xs []     = xs
  appendS (x:xs) ys = x : (appendS xs ys)
  takeS             = take
  dropS = drop
  showtS s          = case s of
                       [] -> EMPTY
                       [x] -> ELT x
                       (x:xs) -> let n = lengthS s
                                     (l, r) = (takeS (div n 2) s) ||| (dropS (div n 2) s)
                                 in NODE l r
  showlS s          = case s of
                       [] -> NIL
                       (x:xs) -> CONS x xs
  joinS             = concat
  reduceS f b s     = case s of
                        [] -> b
                        [x] -> f b x
                        xs -> reduceS f b (contractS f s)
  scanS f e s       = case s of
                        [] -> (singletonS e, e)
                        [x] -> let funApplied = f e x
                               in (appendS (singletonS e) (singletonS funApplied), funApplied)
                        (x:y:xs) -> let (s', reduceResult) = scanS f e (contractS f s)
                                    in (expandS f s s', reduceResult)
  fromList          = id