import Seq
import ArrSeq
import qualified Arr as A

--1 a
promedios :: Seq s => s Int -> s Float
promedios s = tabulateS f (lengthS s) where
                (l, r) = scanS (+) 0 s
                f  i | i < lengthS s = (fromIntegral (nthS l i) :: Float) / (fromIntegral i :: Float)
                     | otherwise = (fromIntegral r :: Float) / (fromIntegral i :: Float)

--b
mayoresAux :: Seq s => s Int -> s Int
mayoresAux s = let s' = fst (scanS max (nthS s 0) s)
               in tabulateS (\i -> if nthS s (i+1) > nthS s' i then 1 else 0) (lengthS s)


mayores :: Seq s => s Int -> Int
mayores s = reduceS (+) 0 (mayoresAux s)

--2
matrixMult :: (Int, Int, Int, Int) -> (Int, Int, Int, Int) -> (Int, Int, Int, Int)
matrixMult (a,b,c,d) (w,x,y,z) = (a*w+b*y, a*x+b*z, c*w+d*y, c*x+d*z)

fibSeq :: Seq s => Int -> s Int
fibSeq n = mapS (\(a,b,c,d) -> a) (fst (scanS matrixMult (1, 1, 1, 0) (tabulateS (\_ -> (1,1,1,0)) n)))


--3
reverseS :: Seq s => s a -> s a
reverseS s = tabulateS (\i -> nthS s (lengthS s - i - 1)) (lengthS s)


aguaHistAux :: Seq s => s Int -> s Int
aguaHistAux s = (tabulateS agua (n - 2)) where
                  n = lengthS s
                  maxLs = (dropS 1) $ fst $ scanS max (nthS s 0) s
                  maxRs = (dropS 1) $ fst $ scanS max (nthS s (n - 1)) (reverseS s)
                  agua i = max 0 (min (nthS maxLs i) (nthS maxRs (n - i - 2)) - nthS s i)

aguaHist :: Seq s => s Int -> Int
aguaHist s = reduceS (+) 0 (aguaHistAux s)

ejemplo = fromList [2,3,4,7,5,2,3,2,6,4,3,5,2,1] :: A.Arr Int

--4
data Paren = Open | Close

matchP :: Seq s => s Paren -> (Int, Int)
matchP s = dyc s v b c where
             v = (0,0)
             b Open = (1,0)
             b Close = (0,1)
             c (lo, lc) (ro, rc) = (max 0 (lo-rc) + ro, max 0 (rc-lo) + lc)

matchParen :: Seq s => s Paren -> Bool
matchParen s = matchP s == (0, 0)


--
matchParenS :: Seq s => s Paren -> Bool
matchParenS s = (red == 0) && (reduceS min (nthS seq 0) seq) == 0 where
                b Open = 1
                b Close = -1
                (seq, red) = scanS (+) 0 (mapS b s)


--5
mcss :: Seq s => s Int -> Int
mcss sq = fst' $ dyc sq v b c where
            fst' (a, _, _, _) = a
            v = (0, 0, 0, 0)
            b i = let i' = max 0 i in (i', i', i', i)
            c (r, p, s, t) (r', p', s', t') = (maximum [r, r', s+p'], max p (t+p'), max s' (s+t'), t+t')

sccmlAux :: Seq s => s Int -> s Int
sccmlAux s = tabulateS f (lengthS s) where
               f 0 = 0
               f n | nthS s n > nthS s (n-1) = 1
                   | otherwise = negate (lengthS s)

sccml :: Seq s => s Int -> Int
sccml s = mcss $ sccmlAux s

sccmlSAux :: Seq s => s Int -> s (Int, Int)
sccmlSAux s = tabulateS (\i -> (i, i)) (lengthS s)

sccmlSAux2 :: Seq s => s (Int, Int) -> s Int
sccmlSAux2 s = mapS (\(i, j) -> j-i ) s

sccmlS :: Seq s => s Int -> Int
sccmlS s = reduceS max 0 $ sccmlSAux2 $ fst $ scanS f (0, 0) (sccmlSAux s) where
             f (i, j) (i', j') | (j + 1 == i') && ((nthS s j) < (nthS s i')) = (i, j')
                               | otherwise = (i', j')

ejemplo1 = fromList [9,3,5,1,2,3,4,5,6,1] :: A.Arr Int
ejemplo2 = fromList [5,6,3,3,5,7,8,9,10,1,9] :: A.Arr Int
ejemplo3 = fromList [1,4,6,7,8,9,10,3] :: A.Arr Int


--6
multiplos :: Seq s => s Int -> s (s Int)
multiplos s = tabulateS f l where
                l = lengthS s
                f i = tabulateS (f' i) (l - i - 1)
                f' i j = if mod (nthS s i) (nthS s (l-j-1)) == 0 then 1 else 0

cantMultiplos :: Seq s => s Int -> Int
cantMultiplos s = reduceS (+) 0 (mapS (reduceS (+) 0) (multiplos s))

ejemplo4 = fromList [12,4,6,3,2] :: A.Arr Int
ejemplo5 = fromList [4,6,2] :: A.Arr Int
ejemplo6 = fromList [1,2,3,4,5] :: A.Arr Int

--7
merge :: Seq s => (a -> a -> Ordering) -> s a -> s a -> s a
merge ord s1 s2 = mapS (run initState) t where
                    (t, total) = scanS ap initComp (tabulateS (const f) l)
                    ap f g = \s -> let (c,s') = f s in g s'
                    initState = (0,0)
                    initComp s = case ord (nthS s1 0) (nthS s2 0) of
                                   LT -> (nthS s1 0, (1,0))
                                   _ -> (nthS s2 0, (0,1))
                    l = lengthS s1 + lengthS s2
                    run st c = fst (c st)
                    f (n, m) = let left = (nthS s1 n, (n+1, m))
                                   right = (nthS s2 m, (n, m+1))
                               in if n >= lengthS s1 then right
                                  else if m >= lengthS s2 then left
                                  else case ord (nthS s1 n) (nthS s2 m) of
                                    LT -> left
                                    _ -> right

sort :: Seq s => (a -> a -> Ordering) -> s a -> s a
sort f s = dyc s emptyS singletonS (merge f)

maxE :: Seq s => (a -> a -> Ordering) -> s a -> a
maxE f s = reduceS f' (nthS s 0) s where
             f' a b = if f a b == GT then a else b

maxSaux :: Seq s => s a -> s (a, Int)
maxSaux s = tabulateS (\i -> (nthS s i, i)) (lengthS s)

maxS :: Seq s => (a -> a -> Ordering) -> s a -> Int
maxS f s = snd $ reduceS f' (nthS s 0, 0) (maxSaux s) where
             f' l@(a, _) r@(b, _) = if f a b == GT then l else r

group:: Seq s => (a -> a -> Ordering) -> s a -> s a
group f s = joinS (tabulateS f' (lengthS s)) where
              f' 0 = singletonS (nthS s 0)
              f' n = case f (nthS s n) (nthS s (n-1)) of
                       EQ -> emptyS
                       _ -> singletonS (nthS s n)

appendS' :: Seq s => s a -> s a -> s a -> s a
appendS' l m r = tabulateS (choose l m r) (lengthS l + lengthS m + lengthS r) where
                    choose s s' s'' i | i < lengthS s = nthS s i
                                      | i < lengthS s + lengthS s' = nthS s' (i - lengthS s)
                                      | otherwise = nthS s'' (i - lengthS s - lengthS s')

collect :: (Seq s, Ord a) => s (a, b) -> s (a, s b)
collect s = dyc (sort myord s) v b c where
                    myord (a, _) (a', _) = compare a a'
                    v = emptyS
                    b (a, b) = singletonS (a, singletonS b)
                    c l r = case lengthS l of
                              0 -> r
                              n -> case lengthS r of
                                     0 -> l
                                     n' -> if myord (nthS l (n-1)) (nthS r 0) == EQ
                                           then let k = fst $ nthS l (n-1)
                                                    vs = snd $ nthS l (n-1)
                                                    vs' = snd $ nthS r 0
                                                in appendS' (takeS (n-1) l) (singletonS (k, appendS vs' vs)) (dropS 1 r)
                                           else appendS l r
ejemplo7 = fromList [(2, "a"), (2, "b"), (1, "c"), (2, "d")] :: A.Arr (Int, String)

mapCollectReduceS :: (Ord k, Seq s) => (doc -> s (k, v)) -> ((k, s v) -> b) -> s doc -> s b
mapCollectReduceS apv red s = let pairs = joinS (mapS apv s)
                                  groups = collect pairs
                              in mapS red groups

datosIngreso :: Seq s => s (String, s Int) -> s (Int, Int)
datosIngreso s = mapCollectReduceS apv red s where
                   apv :: Seq s => (String, s Int) -> s (Int, Int)
                   apv (_, s') = let promedio = div (reduceS (+) 0 s') (lengthS s')
                                     maxima = reduceS max (nthS s' 0) s'
                                 in if promedio >= 70 then singletonS (0, maxima)
                                    else if promedio >= 50 then singletonS (1, maxima)
                                         else singletonS (2, maxima)
                   red :: Seq s => (Int, s Int) -> (Int, Int)
                   red (est, s'') = (lengthS s'', reduceS max (nthS s'' 0) s'')

ejemplo8 = fromList [("juan", fromList [49]),("agustin", fromList [71]),("pedro", fromList [71])] :: A.Arr (String, A.Arr Int)

--9
countCaract :: Seq s => s (s Char) -> s (Char, Int)
countCaract s = mapCollectReduceS apv red s where
                  apv :: Seq s => s Char -> s (Char, Int)
                  apv doc = mapS (\c -> (c , 1)) doc
                  red :: Seq s => (Char, s Int) -> (Char, Int)
                  red (c, s) = (c, lengthS s)

ejemplo9 = fromList [fromList "aabaa", fromList "bbcbb", fromList "ccacc"] :: A.Arr (A.Arr Char)
{-
huffman :: Seq s => s (s Char) -> s (Int, s Char)
-}
