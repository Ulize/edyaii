type Pos = Int
data Linea = A{pos :: Pos, str :: [Char]} deriving Show
vacia :: Linea
vacia = A{pos = 0, str = []}

moverIzq :: Linea -> Linea
moverIzq (A 0 s) = A 0 s
moverIzq (A c s) = A (c-1) s

moverDer :: Linea -> Linea
moverDer (A c s) | c+1 <= length(s) = A (c+1) s
                 | otherwise = A c s

moverIni :: Linea -> Linea
moverIni (A _ s) = A 0 s

moverFin :: Linea -> Linea
moverFin (A _ s) = A (length(s)) s

insertar :: Char -> Linea -> Linea
insertar c (A 0 s) = A 1 (c:s)
insertar ch (A cu s) = A cu ((take cu s)++[ch]++(drop cu s))

borrar :: Linea -> Linea
borrar (A 0 s) = (A 0 s)
borrar (A cu linea) = A cu ((take (cu-1) linea)++(drop cu linea))
