data Exp = Lit Int | Add Exp Exp | Sub Exp Exp | Prod Exp Exp | Div Exp Exp

seval :: Exp -> Maybe Int
seval (Lit a) = (Just a)
seval (Add a b) = case ((seval a), (seval b)) of
                                                      (Nothing, v2) -> Nothing
                                                      (v1, Nothing) -> Nothing
                                                      (Just v1, Just v2) -> Just (v1+v2)

seval (Sub a b) = case ((seval a), (seval b)) of
                                                      (Nothing, v2) -> Nothing
                                                      (v1, Nothing) -> Nothing
                                                      (Just v1, Just v2) -> Just (v1-v2)

seval (Prod a b) = case ((seval a), (seval b)) of
                                                      (Nothing, v2) -> Nothing
                                                      (v1, Nothing) -> Nothing
                                                      (Just v1, Just v2) -> Just (v1*v2)

seval (Div a b) = case ((seval a), (seval b)) of
                                                      (Nothing, v2) -> Nothing
                                                      (v1, Nothing) -> Nothing
                                                      (Just v1, Just v2) -> if v2 == 0 then Nothing else Just (div v1 v2)