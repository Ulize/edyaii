--a
import Data.Char
data Exp = Lit Int | Add Exp Exp | Sub Exp Exp | Prod Exp Exp | Div Exp Exp deriving Show

parseRPNAux :: String -> [Exp] -> Exp
parseRPNAux [] [e] = e
parseRPNAux (x:xs) s | x == ' ' = parseRPNAux xs s
                     | isDigit x = parseRPNAux xs ((Lit (digitToInt x)):s)
                     | x == '+' = parseRPNAux xs ((Add (head (tail s)) (head s)):(tail(tail s)))
                     | x == '-' = parseRPNAux xs ((Sub (head (tail s)) (head s)):(tail(tail s)))
                     | x == '*' = parseRPNAux xs ((Prod (head (tail s)) (head s)):(tail(tail s)))
                     | x == '/' = parseRPNAux xs ((Div (head (tail s)) (head s)):(tail(tail s)))

parseRPN :: String -> Exp
parseRPN s = parseRPNAux s []

--b
eval :: Exp -> Int
eval (Lit a) = a
eval (Add a b) = (eval a) + (eval b)
eval (Sub a b) = (eval a) - (eval b)
eval (Prod a b) = (eval a) * (eval b)
eval (Div a b) = div (eval a) (eval b)

evalRPN :: String -> Int
evalRPN s = eval (parseRPN s)