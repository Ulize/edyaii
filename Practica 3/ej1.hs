data Color = A{red :: Float, green :: Float, blue :: Float} deriving Show
mezclar :: Color -> Color -> Color
mezclar (A r1 g1 b1) (A r2 g2 b2) = A{red = (r1+r2)/2, green = (g1+g2)/2, blue = (b1+b2)/2}