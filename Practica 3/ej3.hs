--a
data CList a = EmptyCL | CUnit a | Consnoc a (CList a) a deriving Show

headCL :: CList a -> a
headCL EmptyCL = undefined
headCL (CUnit a) = a
headCL (Consnoc a _ _) = a

rearmar :: CList a -> a -> CList a
rearmar EmptyCL v = CUnit v
rearmar (CUnit a) v = Consnoc a EmptyCL v
rearmar (Consnoc v1 s v2) v = Consnoc v1 (rearmar s v2) v
tailCL :: CList a -> CList a
tailCL EmptyCL = undefined
tailCL (CUnit a) = EmptyCL
tailCL (Consnoc _ s v) = rearmar s v

isEmptyCL :: CList a -> Bool
isEmptyCL EmptyCL = True
isEmptyCL _ = False

isCUnit :: CList a -> Bool
isCUnit (CUnit a) = True
isCUnit _ = False

--b
reverseCL EmptyCL = EmptyCL
reverseCL (CUnit a) = CUnit a
reverseCL (Consnoc v1 s v2) = Consnoc v2 (reverseCL s) v1

--c
inits :: CList a -> CList (CList a)
inits EmptyCL = EmptyCL
inits (CUnit u) = (CUnit (CUnit u))
inits (Consnoc v1 s v2) = Consnoc (Consnoc v1 s v2) (lasts (reverseCL (tailCL (tailCL (reverseCL (Consnoc v1 s v2)))))) (reverseCL (tailCL (reverseCL (Consnoc v1 s v2))))

--d
lasts :: CList a -> CList (CList a)
lasts EmptyCL = EmptyCL
lasts (CUnit u) = CUnit (CUnit u)
lasts (Consnoc v1 s v2) = Consnoc (Consnoc v1 s v2) (inits (tailCL (tailCL (Consnoc v1 s v2)))) (tailCL (Consnoc v1 s v2))

--e
concatDos :: CList a -> CList a -> CList a
concatDos EmptyCL a = a
concatDos a EmptyCL = a
concatDos a b = concatDos(rearmar a (headCL b)) (tailCL b)

concatCL :: CList (CList a) -> CList a
concatCL EmptyCL = EmptyCL
concatCL (CUnit s) = s
concatCL ss = concatDos(headCL ss) (concatCL (tailCL ss))